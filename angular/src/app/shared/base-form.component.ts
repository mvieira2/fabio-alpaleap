import { Component, Injectable, OnInit } from '@angular/core';
import { FormGroup, FormArray } from '@angular/forms';
import swal from 'sweetalert2';

@Component({
  template: ''
})
export abstract class BaseFormComponent implements OnInit {
  form: FormGroup;

  constructor() {}

  ngOnInit() {}

  abstract submit(): any;

  onSubmit() {
    if (this.form.valid) {
      this.submit();
    } else {
      swal.fire({
        icon: 'warning',
        title: '',
        html: 'Preencha os campos para acessar! <br> Fill in the fields to access!',
        showCloseButton: true,
        confirmButtonText: 'OK'
      });
      this.verifyFormValidations(this.form);
    }
  }

  verifyFormValidations(formGroup: FormGroup | FormArray) {
    Object.keys(formGroup.controls).forEach(field => {
      const controle = formGroup.get(field);
      controle.markAsDirty();
      controle.markAsTouched();
      if (controle instanceof FormGroup || controle instanceof FormArray) {
        this.verifyFormValidations(controle);
      } else {
        if (!controle.valid) {

          console.log(controle);
          console.log(field);
        }
      }
    });
  }

  reset() {
    this.form.reset();
  }

  verifyRequired(field: string) {
    return this.form.get(field).hasError('required') && (this.form.get(field).touched || this.form.get(field).dirty);
  }

  applyCssError(field: string) {
    if (field == 'passwordConfirmation') {
      let hasErrors = this.form.errors ? true : false;
      let hasDanger = hasErrors && this.form.errors.notSame ? true : false;

      return {
        invalid: hasDanger ? true : this.verifyInvalidAndTouched(field, null)
      };
    } else {
      return {
        invalid: this.verifyInvalidAndTouched(field, null)
      };
    }
  }

  verifyInvalidAndTouched(field: any, formParam?: string) {
    let formView: any = '';
    let currentForm = formParam || this.form;
    (currentForm && !formParam) || formParam == 'form' ? (formView = 'form') : (formView = formParam);
    let value = this[formView].get(field) && this[formView].get(field).value;
    let trimmedValue;

    try {
      trimmedValue = value ? (isNaN(value) ? value.trim() : value) : null;
    } catch (error) {
      trimmedValue = '';
    }

    if (value == '' || trimmedValue == '') {
      this[formView].get(field).setErrors({ empty: true });
    }

    return this[formView].get(field) && !this[formView].get(field).valid && (this[formView].get(field).touched || this[formView].get(field).dirty);
  }
}
