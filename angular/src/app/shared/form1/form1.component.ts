import { Component, OnInit } from '@angular/core';
import { BaseFormComponent } from '../base-form.component';

import { HttpClient} from '@angular/common/http';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { environment } from 'src/environments/environment';


import { NgxSpinnerService } from "ngx-bootstrap-spinner";

@Component({
  selector: 'app-form1',
  templateUrl: './form1.component.html',
  styleUrls: ['./form1.component.scss']
})
export class Form1Component extends BaseFormComponent implements OnInit  {

  form: FormGroup;
  link : string = environment.LINK;
  api_url : string = environment.API_URL;
  showLink : boolean = false;


  infos : Array<any> = null;

  constructor(
    private formBuilder: FormBuilder,
    private httpClient: HttpClient,
    private spinner: NgxSpinnerService,) { 
    super();
  }

  ngOnInit(): void {

    this.getInfos();

    this.form = this.formBuilder.group({
      name: [null, Validators.required],
      email: [null, [Validators.email, Validators.required]]
    });

  }

  getInfos() {
    this.spinner.show();

    this.httpClient.get(this.api_url + "/infos").subscribe((result : any) => {
      if(result.error) return;

      this.infos = result.data;

    },error => {

      this.spinner.hide();
      console.log(error)
    }, () => {
      this.spinner.hide();
    });
  
  }

  submit() {
    this.spinner.show();

    this.httpClient.post(this.api_url + "/save", this.form.value).subscribe(result => {

      this.showLink = true;

    },error => {
      this.showLink = false;
      this.spinner.hide();
      
    }, () => {
      this.spinner.hide();
    });
  
  }

}
