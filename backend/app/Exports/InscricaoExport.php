<?php

namespace App\Exports;


use App\Models\Inscricao;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class InscricaoExport implements FromCollection, WithHeadings
{
    public function __construct()
    {

    }

    public function collection()
    {
        return Inscricao::get()->makeHidden(['status']);

    }

    public function headings(): array
    {
        return ["ID","NOME COMPLETO / FULL NAME", "EMAIL", "CRIADO EM / CREATED AT", "ATUALIZADO EM / UPDATED AT"];
    }

}