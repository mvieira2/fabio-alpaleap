<?php

namespace App\Http\Controllers;

use App\Models\Inscricao;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class InscricaoController extends Controller
{


    function __construct()
    {
    }

    public function save(Request $request)
    {

        $validator = Validator::make($request->all(), [
            "name"  => 'required|string|max:255',
            "email"  => 'required|string|max:255',
        ]);

        if ($validator->fails()) {
            return response()->json(["error" => true, "errorMessage" => "Dados inválidos"], 400);
        }


        $inscricao = Inscricao::updateOrCreate(
            ["email" => $request->input("email")],
            ["name" => $request->input("name")]
        );

        if ($inscricao) {
            return response()->json(["error" => false, "successMessage" => "Inscrição efetuada com sucesso!"], 200);
        }
        return response()->json(["error" => true, "errorMessage" => "Não foi possível te inscrever no momento."], 400);
    }

    public function infos(Request $request)
    {

        $data = [
            [
                "blocoName" =>  "Dia 4/11 - Quinta | Day 11/4 - Thursday",
                "listInfos" => [
                    [
                        "info1" => "8H30 - 9H20",
                        "info2" => "<b>CEO BETO FUNARI -ESTRATÉGIA /  STRATEGY 5 ANOS + CULTURA / CULTURE</b>",
                        "info3" => ""
                    ],
                    [
                        "info1" => "9H20 - 9H45",
                        "info2" => "<b>CRESCIMENTO | GROWTH: MAFÊ ALBUQUERQUE, VP MKT GLOBAL HAVAIANAS + LEANDRO MEDEIROS VP DE GROWTH & OSKLEN. </b>",
                        "info3" => ""
                    ],
                    [
                        "info1" => "9H45 - 10H",
                        "info2" => "<span>(BREAK 15)</span>",
                        "info3" => ""
                    ],
                    [
                        "info1" => "10H - 10H25",
                        "info2" => "<b>DIGITAL | JORGE RAMALHO, VP DE TECNOLOGIA + GILSON VILELA, CO-FOUNDER IOASYS </b>",
                        "info3" => ""
                    ],
                    [
                        "info1" => "10H25 - 10H50",
                        "info2" => "<b>INOVAÇÃO |  INNOVATION: SIMONE FRANCO, VP SUPPLY CHAIN + EDSON RUBIÃO, VP INDUSTRIAL</b>",
                        "info3" => ""
                    ],
                    [
                        "info1" => "10H50 - 11H15",
                        "info2" => "<b>SUSTENTABILIDADE | SUSTAINABILITY: ZEZÉ DE MARTINI, HEAD DE SUSTENTABILIDADE E REPUTAÇÃO</b>",
                        "info3" => ""
                    ],
                    [
                        "info1" => "11H15 - 11H30",
                        "info2" => "<span>(BREAK 15)</span>",
                        "info3" => ""
                    ],
                    [
                        "info1" => "11H30 - 12H15",
                        "info2" => "<b>Q&A </b>",
                        "info3" => ""
                    ],
                    [
                        "info1" => "12H15 - 13H",
                        "info2" => "<b>ALPA'S PARTNER RITUAL</b>",
                        "info3" => ""
                    ],
                    [
                        "info1" => "13H - 13H10",
                        "info2" => "<b>CLOSING</b>",
                        "info3" => ""
                    ],
                ]
            ],
            [
                "blocoName" =>  "Dia 5/11 Sexta | Day 11/5 - Friday",
                "listInfos" => [
                    [
                        "info1" => "8H30 - 8H40",
                        "info2" => "<b>CEO BETO FUNARI - RECAP</b>",
                        "info3" => ""
                    ],
                    [
                        "info1" => "8H40 - 9H05",
                        "info2" => "<b>GLOBAL | ANA BÓGUS, VP BMU HAVAIANAS BRASIL + FRED LEVY, VP BMU HAVAIANAS INTERNACIONAL </b>",
                        "info3" => ""
                    ],
                    [
                        "info1" => "9H05 - 9H10",
                        "info2" => "<span>(BREAK 5)</span>",
                        "info3" => ""
                    ],
                    [
                        "info1" => "9H10 - 11H",
                        "info2" => "<b>ANNI TOWNEND & LUCY KIDD | EMPATIA E COLABORAÇÃO | EMPATHY & COLLABORATION </b>",
                        "info3" => ""
                    ],
                    [
                        "info1" => "11H - 11H10",
                        "info2" => "<span>(BREAK 10)</span>",
                        "info3" => ""
                    ],
                    [
                        "info1" => "11H10 - 12H",
                        "info2" => "<b>ANNI TOWNEND & LUCY KIDD | EMPATIA E COLABORAÇÃO | EMPATHY & COLLABORATION </b>",
                        "info3" => ""
                    ],
                    [
                        "info1" => "12H - 12H05",
                        "info2" => "<span>(BREAK 5)</span>",
                        "info3" => ""
                    ],
                    [
                        "info1" => "12H05 - 12H50",
                        "info2" => "<b>CELEBRAÇÃO + ENCERRAMENTO COM PERSPECTIVAS FUTURAS | CELEBRATION + CLOSING WITH LOOKING FORWARD </b>",
                        "info3" => ""
                    ],
                ]
            ]
        ];

        return response()->json(["error" => false, "data" => $data], 200);
    }
}
