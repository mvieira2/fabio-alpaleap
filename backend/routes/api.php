<?php

use App\Exports\InscricaoExport;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/



Route::post('save', 'App\Http\Controllers\InscricaoController@save');

Route::get('infos', 'App\Http\Controllers\InscricaoController@infos');

Route::get('excel', function(){
    
    return Excel::download(new InscricaoExport(), 'excel.xlsx');
});
